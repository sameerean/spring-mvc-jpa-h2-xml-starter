<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>Spring MVC JPA Starter: List of Users</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<h1>Spring MVC + JPA (with H2 DB) Starter Project</h1>

		<P>The time on the server is ${serverTime}.</P>

		<table class="table table-hover">
			<tr>
				<td><a href="user">User List</a></td>
			</tr>
		</table>
	</div>
</body>
</html>
