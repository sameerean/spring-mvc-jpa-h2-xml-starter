<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<title>Spring MVC JPA Starter: View User</title>
</head>
<body>
	<h1 class="text-center">User Details</h1>
	<hr>
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">User...</h3>
			</div>
			<div class="panel-body">

				<%-- <form:form action="new" method="post" commandName="user"> --%>
					<div class="form-group">
						<label for="txtUserName">User-name</label>
						<p>${user.userName}</p>
					</div>
					<div class="form-group">
						<label for="txtFirstName">First Name</label> 
						<p>${user.firstName}</p>
					</div>
					<div class="form-group">
						<label for="txtLastName">Last Name</label> 
						<p>${user.lastName}</p>
					</div>
					<div class="form-group">
						<label for="calDob">Date of Birth</label> 
						<p>${user.dateOfBirth}</p>
					</div>
					<div class="form-group">
						<label for="txtAddress">Address</label> 
						<p>${user.address}</p>
					</div>
					<div class="form-group">
						<label for="txtPhone">Phone</label> 
						<p>${user.phoneNumber}</p>
					</div>
					<div class="form-group">
						<label for="txtEmail">Email</label> 
						<p>${user.email}</p>
					</div>
					<a class="btn btn-success glyphicon glyphicon-pencil" href='<c:url value="/user/edit/${user.pid}"/>'>Edit</a>
					<a href="../user" class="btn btn-primary">Back to User list</a>
				<%-- </form:form> --%>
			</div>
		</div>

	</div>
</body>
</html>
