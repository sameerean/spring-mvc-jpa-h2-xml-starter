<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<title>Spring MVC JPA Starter: New User</title>
<script type="text/javascript">
	function deleteUser(pid) {
		if (confirm("Delete... PID = " + pid + " ??? ")) {
			with (document.forms[0]) {
				method = "POST";
				action = "../delete/" + pid;
				submit();
			}
		}

	}
</script>
</head>
<body>
	<h1 class="text-center">Edit User</h1>
	<hr>
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Enter user details here..</h3>
			</div>
			<div class="panel-body">

				<form:form action="${user.pid}" method="post" commandName="user">
					<div class="form-group">
						<label for="txtUserName">User-name</label>
						<form:input path="userName" class="form-control" id="txtUserName"
							placeholder="User Name" />
					</div>
					<div class="form-group">
						<label for="txtFirstName">First Name</label>
						<form:input path="firstName" class="form-control"
							id="txtFirstName" placeholder="First Name" />
					</div>
					<div class="form-group">
						<label for="txtLastName">Last Name</label>
						<form:input path="lastName" class="form-control" id="txtLastName"
							placeholder="Last Name" />
					</div>
					<div class="form-group">
						<label for="calDob">Date of Birth</label>
						<form:input path="dateOfBirth" class="form-control" id="calDob"
							placeholder="dd/MM/yyyy" />
					</div>
					<div class="form-group">
						<label for="txtAddress">Address</label>
						<form:textarea path="address" class="form-control"
							id="taxtAddress" placeholder="Address" />
					</div>
					<div class="form-group">
						<label for="txtPhone">Phone</label>
						<form:input path="phoneNumber" class="form-control" id="txtPhone"
							placeholder="000000000000" />
					</div>
					<div class="form-group">
						<label for="txtEmail">Email</label>
						<form:input path="email" class="form-control" id="txtEmail"
							placeholder="youremail@yourcompany.com" />
					</div>
					<button type="submit" class="btn btn-success">Save</button>
					<a href='<c:url value="/user/${user.pid}"/>'
						class="btn btn-primary">Cancel</a>
					<a href="javascript:deleteUser('${user.pid}');"
						class="btn btn-danger">Delete</a>
				</form:form>
			</div>
		</div>

	</div>
</body>
</html>
