<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<title>Spring MVC JPA Starter: List of Users</title>
</head>
<body>
<div class="container">
	<h1>List of Users</h1>

	<hr />
	<p class="text-right">
		<a class="btn btn-primary" href="user/new" role="button">Create New
			User</a>
	</p>
	<table class="table table-hover">
		<thead>
			<tr>
				<th>PID</th>
				<th>User Name</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Date Of Birth</th>
			</tr>
		</thead>
		<tbody>
			<c:if test="${not empty users}">

				<ul>
					<c:forEach var="user" items="${users}">
						<tr>
							<td><a href='<c:url value="/user/${user.pid}"/>'>${user.pid}</a></td>
							<td>${user.userName}</td>
							<td>${user.firstName}</td>
							<td>${user.lastName}</td>
							<td>${user.dateOfBirth}</td>
						</tr>
					</c:forEach>
				</ul>

			</c:if>

		</tbody>
	</table>
</div>
</body>
</html>
