package io.sameerean.starterprojects.service;

import java.util.List;

import io.sameerean.starterprojects.data.entity.User;

public interface UserService {

	void createUser(User user);
	void updateUser(User user);
	void deleteUser(User user);
	User findUserById(Integer id);
	User findUserByUserName(String userName);
	User findUserByPid(String pid);
	List<User> findAllUsers();
}
