package io.sameerean.starterprojects.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.sameerean.starterprojects.data.dao.UserDAO;
import io.sameerean.starterprojects.data.entity.User;
import io.sameerean.starterprojects.service.UserService;

@Transactional
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;

	@Override
	public void createUser(User user) {
		user.setPid("usr" + new Date().getTime());
		this.userDAO.save(user);
	}

	@Override
	public void updateUser(User user) {
		this.userDAO.save(user);
	}

	@Override
	public void deleteUser(User user) {
		this.userDAO.delete(user);
	}

	@Transactional(readOnly = true)
	@Override
	public User findUserById(Integer id) {
		return this.userDAO.findOne(id);
	}

	@Transactional(readOnly = true)
	@Override
	public User findUserByUserName(String userName) {
		return this.userDAO.findByUserName(userName);
	}

	@Transactional(readOnly = true)
	@Override
	public User findUserByPid(String pid) {
		return this.userDAO.findByPid(pid);
	}

	@Transactional(readOnly = true)
	@Override
	public List<User> findAllUsers() {
		return this.userDAO.findAll();
	}

}
