package io.sameerean.starterprojects.web.controller;

import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import io.sameerean.starterprojects.data.entity.User;
import io.sameerean.starterprojects.service.UserService;

/**
 * Handles requests for user related pages.
 */
@Controller
@RequestMapping("/user")
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@RequestMapping(method = RequestMethod.GET)
	public String listAll(Locale locale, Model model) {

		model.addAttribute("users", userService.findAllUsers());
		return "user/list";
	}

	@RequestMapping(path = "/new", method = RequestMethod.GET)
	public String newUserForm(Model model) {

		User user = new User();
		user.setDateOfBirth(new Date());
		model.addAttribute("user", user);
		return "user/new";
	}

	@RequestMapping(path = "/new", method = RequestMethod.POST)
	public String saveNewUser(@ModelAttribute("user") User user, Model model) {

		userService.createUser(user);
		return "redirect:/user";
	}

	@RequestMapping(path = "/{pid}", method = RequestMethod.GET)
	public String viewUser(@PathVariable("pid") String pid, Model model) {

		model.addAttribute("user", userService.findUserByPid(pid));
		return "user/view";
	}

	@RequestMapping(path = "/edit/{pid}", method = RequestMethod.GET)
	public String editUser(@PathVariable("pid") String pid, Model model) {
		
		model.addAttribute("user", userService.findUserByPid(pid));
		return "user/edit";
	}

	@RequestMapping(path = "/edit/{pid}", method = RequestMethod.POST)
	public String updateUser(@PathVariable("pid") String pid, @ModelAttribute("user") User user, Model model) {


		User existingUser = userService.findUserByPid(pid);
		existingUser.setUserName(user.getUserName());
		existingUser.setFirstName(user.getFirstName());
		existingUser.setLastName(user.getLastName());
		existingUser.setDateOfBirth(user.getDateOfBirth());
		existingUser.setAddress(user.getAddress());
		existingUser.setPhoneNumber(user.getPhoneNumber());
		existingUser.setEmail(user.getEmail());
		
		userService.updateUser(existingUser);

		model.addAttribute("user", userService.findUserByPid(pid));
		return "redirect:/user/" + pid;
	}

	@RequestMapping(path = "/delete/{pid}", method = RequestMethod.POST)
	public String deleteUser(@PathVariable("pid") String pid, Model model) {

		User existingUser = userService.findUserByPid(pid);
		
		userService.deleteUser(existingUser);
		return "redirect:/user";
	}
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addCustomFormatter(new DateFormatter("dd/MM/yyyy"));
	}
}
