package io.sameerean.starterprojects.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.sameerean.starterprojects.data.entity.User;

@Repository
public interface UserDAO extends JpaRepository<User, Integer> {

	User findByUserName(String userName);

	User findByPid(String pid);

}
